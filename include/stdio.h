#ifndef STDIO_H
#define STDIO_H
int printf_debug(const char *format, ...);
int printf(const char *format, ...);

#endif /* STDIO_H */
