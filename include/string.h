#include <stddef.h>
void itoa(int num, char *str, int base);
void reverse(char *s);
size_t strlen(char *s);
void *memcpy(void *dest, const void *src, size_t n);
void *memset(void *s, int c, size_t n);
