#include <stdint.h>

inline void outb(uint16_t port, uint8_t byte)
{
    asm volatile ("outb %0, %1" :: "a"(byte), "Nd"(port));
}
inline uint8_t inb(uint16_t port)
{
    uint8_t byte;
    asm volatile ("inb %1, %0" : "=a"(byte) :  "Nd"(port));
    return byte;
}
