global setGdt, reloadSegments, gdtr 

gdtr DW 0 ; For limit storage
     DD 0 ; For base storage
 
setGdt:
   mov   EAX, [esp + 4]
   mov   [gdtr + 2], EAX
   mov   AX, [ESP + 8]
   mov   [gdtr], AX
   lgdt  [gdtr]
   ret

reloadSegments:
    mov AX, 0x10
    mov DS, AX
    mov ES, AX
    mov FS, AX
    mov GS, AX
    mov SS, AX
    jmp 0x08:flush
flush:
    ret
