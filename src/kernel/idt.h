#ifndef IDT_H
#define IDT_H
#include <stdint.h>
// Defines des types
#define IDT_TASKGATE 0x5
#define IDT_16BIT_INTGATE 0x6
#define IDT_16BIT_TRAPGATE 0x7
#define IDT_32BIT_INTGATE 0xE
#define IDT_32BIT_TRAPGATE 0xF
struct IDTEntry {
    uint16_t base0_15;
    uint16_t selector;
    uint8_t zero; // Doit être à zero.
    uint8_t type : 4;
    uint8_t storage_segment : 1;
    uint8_t dpl : 2;
    uint8_t present : 1;
    uint16_t base16_31;
} __attribute__((packed));
struct IDT_Desc {
    uint16_t size;
    uint32_t address;
} __attribute__((packed));
void init_idt_entry(uint32_t base, uint16_t selector, uint8_t type, uint8_t storage_segment, uint8_t dpl, uint8_t present, struct IDTEntry *entry);
void install_idt(uint32_t base);
extern struct IDTEntry tidt[256];
extern struct IDT_Desc idtr;
#endif /* IDT_H */
