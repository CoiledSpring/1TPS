#include <stdio.h>
#include "boot.h"
#include "gdt.h"
#include "idt.h"
#include "pic.h"
#include "tty.h"
#include "drivers/serial.h"
#include "drivers/rtc.h"
void main()
{

}
void early_main()
{
  struct rtc_Date date;
  asm("cli" ::);
  install_gdt();
  serial_init(COM1, 8, 0, NONE, 0);
  tty_init();
  printf_debug("Port série et GDT initialisés!\n");
  install_idt(GDT_SIZE * 8);
  printf_debug("IDT installée!\n");
  init_pic();
  printf_debug("PIC initialisé!\n");
  asm("sti" ::);
  printf_debug("Interruptions activées!\n");
  date = rtc_get_date();
  for(int i=0; i<12; i++)
  {
    tty_current = & ttys[i];
    printf(" _  _____  ____                       ____               _                   \n"
    "/ ||_   _||  _ \\  ___   _ __   _   _ / ___|  _   _  ___ | |_  ___  _ __ ___  \n"
    "| |  | |  | |_) |/ _ \\ | '_ \\ | | | |\\___ \\ | | | |/ __|| __|/ _ \\| '_ ` _ \\ \n"
    "| |  | |  |  __/| (_) || | | || |_| | ___) || |_| |\\__ \\| |_|  __/| | | | | |\n"
    "|_|  |_|  |_|    \\___/ |_| |_| \\__, ||____/  \\__, ||___/ \\__|\\___||_| |_| |_|\n"
    "                               |___/         |___/                           \n");;
    printf("Il est %d:%d, nous sommes le %d/%d.\n", date.hours, date.minutes, date.monthday, date.month);
  }
  change_tty(0);
  while(1) tty_actualise();
}
