	extern c_default_int, c_divide_by_zero, c_int_2, c_nmi, c_clock, c_keyboard
	global asm_default_int, asm_divide_by_zero, asm_int_2, asm_nmi, asm_clock, asm_keyboard
	%macro SAVE_REGS 0
	pushad
	%endmacro
	%macro RESTORE_REGS 0
	popad
	%endmacro
	%macro SEND_EOI_PIC1 0
	mov al, 20h
	out 20h, al
	%endmacro
	%macro SEND_EOI_PIC2 0
	mov al, 20h
	out A0h, al
	out 20h, al
	%endmacro
asm_default_int:
	SAVE_REGS
	call c_default_int
	SEND_EOI_PIC1
	RESTORE_REGS
	iret
asm_divide_by_zero:
	SAVE_REGS
	call c_divide_by_zero
	RESTORE_REGS
	iret
asm_int_2:
	SAVE_REGS
	call c_int_2
	RESTORE_REGS
	iret
asm_nmi:
	SAVE_REGS
	call c_nmi
	RESTORE_REGS
	iret

asm_clock:
	SAVE_REGS
	call c_clock
	SEND_EOI_PIC1
	RESTORE_REGS
	iret
asm_keyboard:
	SAVE_REGS
	call c_keyboard
	SEND_EOI_PIC1
	RESTORE_REGS
	iret
