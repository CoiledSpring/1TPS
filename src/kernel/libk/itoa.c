#include <stdint.h>
void itoa(int n, char *buf, int base)
{
        uint8_t chiffre;
        int i, j;
        i = 0;
        do {
                chiffre = n % base;
                buf[i++] = (chiffre < 10)?(chiffre+'0'):(chiffre-10+'A');
        } while (n /= base);
	buf[i] = 0;
	i--;
        for (j = 0; j < i; j++, i--) {
                chiffre = buf[j];
                buf[j] = buf[i];
                buf[i] = chiffre;
        }
}
