#include <stddef.h>
void *memcpy(void *dest, const void *src, size_t n)
{
    while(n--)
    {
	*(char *)(dest++) = *(char *)(src++);
    }
    return dest;
}
