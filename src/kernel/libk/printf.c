#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <tty.h>
int printf(const char *format, ...)
{
    va_list ap; int i;
    char buffer[15];
    va_start(ap, format);
    for(i=0; format[i] != 0; i++)
    {
	if(format[i] == '%')
	{
	    i++;
	    switch(format[i])
	    {
	    case '%':
	        putchar('%');
		break;
	    case 'd':
		itoa(va_arg(ap, int), buffer, 10);
		printf(buffer);
		break;
	    case 'x':
		itoa(va_arg(ap, int), buffer, 16);
		printf(buffer);
		break;
	    case 'c':
		putchar(va_arg(ap, int));
		break;
	    case 's':
		printf((char *)va_arg(ap, int));
		break;
	    default:
		asm("cli \n"
		    "hlt");
	    }
	}
	else putchar(format[i]);
    }
    va_end(ap);
    return 0;
}
