#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <serial.h>
int printf_debug(const char *format, ...)
{
    va_list ap; int i;
    char buffer[15];
    va_start(ap, format);
    for(i=0; format[i] != 0; i++)
    {
	if(format[i] == '%')
	{
	    i++;
	    switch(format[i])
	    {
	    case '%':
		serial_write('%');
		break;
	    case 'd':
		itoa(va_arg(ap, int), buffer, 10);
		printf_debug(buffer);
		break;
	    case 'x':
		itoa(va_arg(ap, int), buffer, 16);
		printf_debug(buffer);
		break;
	    case 'c':
		serial_write(va_arg(ap, int));
		break;
	    case 's':
		printf_debug((char *)va_arg(ap, int));
		break;
	    default:
		asm("cli \n"
		    "hlt");
	    }
	}
	else serial_write(format[i]);
    }
    va_end(ap);
    return 0;
}
