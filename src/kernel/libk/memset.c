#include <string.h>
void *memset(void *s, int c, size_t n)
{
    s = (char *)s;
    while(n--)
    {
	*(char *)(s) = c;
	s++;
    }
    return s;
}
