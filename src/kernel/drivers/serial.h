#ifndef SERIAL_H
#define SERIAL_H
#include <stdint.h>
enum COM_PORT { COM1=0x3f8, COM2=0x2f8 };
enum PARITY { NONE=0, ODD=1, EVEN=3, MARK=5, SPACE=6 };
uint16_t serial_port_currently_in_use;
uint8_t serial_byte_max_to_send;
void serial_init(enum COM_PORT com, uint8_t count, uint8_t stop_bits, enum PARITY parity, uint8_t interrupt);
int serial_is_it_received();
int serial_is_it_empty();
uint8_t serial_read();
void serial_write(char byte);
#endif /* SERIAL_H */
