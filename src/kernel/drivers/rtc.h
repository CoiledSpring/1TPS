#ifndef RTC_H
#define RTC_H
#include <stdint.h>
#define from_bcd(x) ((x / 16) * 10 + (x & 0xf))
struct rtc_Date {
    uint8_t seconds;
    uint8_t minutes;
    uint8_t hours;
    uint8_t weekday;
    uint8_t monthday;
    uint8_t month;
    uint8_t year;
    uint8_t century;
};
struct rtc_Date rtc_get_date();

#endif /* RTC_H */
