#include <io.h>
#include <stdio.h>
#include "rtc.h"
#include "cmos.h"

struct rtc_Date rtc_get_date()
{
  struct rtc_Date date;
  uint8_t status_b, pm = 0;
  status_b     = cmos_get_register(0x0b);
  date.seconds = cmos_get_register(0x00);
  date.minutes = cmos_get_register(0x02);
  date.hours   = cmos_get_register(0x04);
  date.weekday = cmos_get_register(0x06);
  date.monthday= cmos_get_register(0x07);
  date.month   = cmos_get_register(0x08);
  date.year    = cmos_get_register(0x09);
  date.century = cmos_get_register(0x32);

  if (~status_b & 0x04) {
    date.seconds = from_bcd(date.seconds);
    date.minutes = from_bcd(date.minutes);
    if ((status_b & 0x02) && (date.hours & 0x80)) { date.hours &= 0x7F; pm = 1; }
    date.hours   = from_bcd(date.hours);
    date.weekday = from_bcd(date.weekday);
    date.monthday= from_bcd(date.monthday);
    date.month   = from_bcd(date.month);
    date.year    = from_bcd(date.year);
    date.century = from_bcd(date.century);
  }

  if ((~status_b & 0x02) && ((date.hours & 0x80) || pm)) {
    date.hours += 12;
  }
  return date;
}
