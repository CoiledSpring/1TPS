#ifndef CMOS_H
#define CMOS_H

#include <stdint.h>

uint8_t cmos_get_register(uint8_t num);
void cmos_set_register(uint8_t num, uint8_t val);

#endif
