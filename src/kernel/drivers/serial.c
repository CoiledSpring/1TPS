#include <io.h>
#include "serial.h"
void serial_init(enum COM_PORT com, uint8_t count, uint8_t stop_bits, enum PARITY parity, uint8_t interrupt)
{
    serial_port_currently_in_use = com;
    serial_byte_max_to_send = count;
    outb(com + 1, interrupt);
    outb(com + 3, 0x80);
    outb(com, 0x03);
    outb(com + 1, 0x00);
    outb(com + 3, (count - 5) + (stop_bits << 2) + (parity << 3));
    outb(com + 2, 0xC7);
    outb(com + 4, 0x0B);
    
}
int serial_is_it_received()
{
    return inb(serial_port_currently_in_use + 5) & 0x01;
}
int serial_is_it_empty()
{
    return inb(serial_port_currently_in_use + 5) & 0x20;
}
uint8_t serial_read()
{
    while(serial_is_it_received() == 0)
	continue;
    return inb(serial_port_currently_in_use);
}
void serial_write(char byte)
{
    while(serial_is_it_empty() == 0)
	continue;
    outb(serial_port_currently_in_use, byte);
}
