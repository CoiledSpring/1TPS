#include <io.h>
#include "cmos.h"

uint8_t cmos_get_register(uint8_t num) {
  outb(0x70, num);
  return inb(0x71);
}

void cmos_set_register(uint8_t num, uint8_t val) {
  outb(0x70, num);
  outb(0x71, val);
}
