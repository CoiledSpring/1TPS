#ifndef PIC_H
#define PIC_H
#define PIC1_PORT 0x20
#define PIC2_PORT 0xA0
#define PIC1_DATA 0x21
#define PIC2_DATA 0xA1
#define PIC_INIT_CODE 0x10
#define PIC_ICW4 0x1
void init_pic();
#endif /* PIC_H */
