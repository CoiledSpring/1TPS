#include <stdint.h>
#include <string.h>
#include "gdt.h"

struct GDTEntry tgdt[GDT_SIZE];
struct GDT_Desc gdtr;
void init_gdt_entry(uint32_t base,
                    uint32_t limite,
                    uint8_t access,
                    uint8_t flags,
                    struct GDTEntry * entry) {
	entry->limite0_15  = limite & 0x0000ffff;
	entry->limite16_19 = limite & 0x000f0000 >> 16;
	entry->base0_15    = base   & 0x0000ffff;
	entry->base16_23   = (base >> 16) & 0xff;
	entry->base24_31   = base   & 0xff000000 >> 24;
	entry->access      = access;
	entry->flags       = flags;
}

void install_gdt() {

	init_gdt_entry(0, 0, 0, 0, &tgdt[0]);
	init_gdt_entry(0, 0xffffffff, GDT_PRESENT | GDT_PRVL_KERNEL | GDT_CODE | GDT_ONLYP | GDT_RW, GDT_PAGE_GRANULARITY | GDT_32BIT_MODE, &tgdt[1]);
	init_gdt_entry(0, 0xffffffff, GDT_PRESENT | GDT_PRVL_KERNEL | GDT_DATA | GDT_GROWUP| GDT_RW, GDT_PAGE_GRANULARITY | GDT_32BIT_MODE, &tgdt[2]);

	gdtr.address = (uint32_t *)&tgdt[0];
	gdtr.size    = sizeof(tgdt) - 1;

	asm("lgdt (gdtr)");

	asm("mov $0x10, %ax   \n"
	    "mov %ax,   %ds   \n"
	    "mov %ax,   %es   \n"
	    "mov %ax,   %fs   \n"
	    "mov %ax,   %gs   \n"
	    "mov %ax,   %ss   \n"
	    "ljmp $0x08, $.next\n"
	    ".next:           \n");

	return;
}
