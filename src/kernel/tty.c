#include "tty.h"
#include <io.h>
#include <string.h>
struct TTY ttys[12];
struct TTY *tty_current;
uint8_t tty_id;
uint16_t *tty_memory;
void tty_init()
{
    int i, j;
    tty_memory = (uint16_t *)VGA_ADDRESS;
    for(i=0; i<12; i++)
    {
	ttys[i].tty_x = 0;
	ttys[i].tty_y = 0;
	ttys[i].tty_color = 0x0F;
	ttys[i].stdin_i = 0;
	ttys[i].tty_cursor = 0;
	for(j=0; j<VGA_WIDTH*VGA_HEIGHT; j++)
	{
	    if(j<250) ttys[i].tty_stdin[j] = 0;
	    ttys[i].tty_buffer[j] = 0x0F20;
	}
    }
    change_tty(0);
}
void putchar(char c)
{
    if (c == '\n')
    {
	tty_current->tty_y++;
	tty_current->tty_x = 0;
    }
    else if(c == '\r')
    {
	tty_current->tty_x = 0;
    }
    else if(c == 127)
    {
	tty_current->tty_x--;
	if(tty_current->tty_x == 255) { tty_current->tty_y--; tty_current->tty_x = (VGA_WIDTH-1); }
	tty_current->tty_buffer[tty_current->tty_y*VGA_WIDTH+tty_current->tty_x] = (tty_current->tty_color<<8) + ' ';
    }
    else
    {
	tty_current->tty_buffer[tty_current->tty_y*VGA_WIDTH+tty_current->tty_x] = (tty_current->tty_color<<8) + c;
	tty_current->tty_x++;
	if (tty_current->tty_x == VGA_WIDTH) { tty_current->tty_x = 0; tty_current->tty_y++; }
    }
    if (tty_current->tty_y == VGA_HEIGHT) { tty_current->tty_y--; tty_current->tty_x = 0; scrollup(); }
    tty_current->tty_cursor = tty_current->tty_y*VGA_WIDTH+tty_current->tty_x;
}
void move_cursor()
{
    outb(0x3D4, 0x0F);
    outb(0x3D5, (tty_current->tty_cursor & 0x00FF));
    outb(0x3D4, 0x0E);
    outb(0x3D5, (tty_current->tty_cursor & 0xFF00)>>8);
}
void change_tty(uint8_t nb)
{
    tty_current = & ttys[nb];
    tty_actualise();
}
void tty_actualise()
{
    memcpy((void *)VGA_ADDRESS, (void *)tty_current->tty_buffer, VGA_WIDTH*VGA_HEIGHT*2);
    move_cursor();
}
void scrollup()
{
    int i;
    memcpy((void *)tty_current->tty_buffer, (void *)(tty_current->tty_buffer + VGA_WIDTH), (VGA_WIDTH*(VGA_HEIGHT-1)-1)*2);
    for(i=0; i<VGA_WIDTH; i++)
	tty_current->tty_buffer[(VGA_HEIGHT-1)*VGA_WIDTH+i] = 0x0F20;
}
