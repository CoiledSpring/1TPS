#include <io.h>
#include "pic.h"

void init_pic()
{
    outb(PIC1_PORT, PIC_INIT_CODE | PIC_ICW4); // On demande la réinitialisation des 2 PICs
    outb(PIC2_PORT, PIC_INIT_CODE | PIC_ICW4);
    outb(PIC1_DATA, 0x20); // On met un offset de 0x20 à PIC1 pour éviter les collisions avec les erreurs du processeur.
    outb(PIC2_DATA, 0x28); // On met les IRQs de PIC2 juste après.
    outb(PIC1_DATA, 0x04); // On annonce à PIC1 la grande nouvelle : Il a un esclave à IRQ2 :D
    outb(PIC2_DATA, 0x02); // On dit à PIC2 que c'est l'esclave de PIC1.
    outb(PIC1_DATA, 0x01); // Mode 8086
    outb(PIC2_DATA, 0x01); // Idem
    //printf_debug("m1 : %x\nm2 : %x\n", m1, m2);
    outb(PIC1_DATA, 0);
    outb(PIC2_DATA, 0);
}
