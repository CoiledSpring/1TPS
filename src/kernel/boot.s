	extern early_main
	
	MALIGN equ 0b1
	MEMINFO equ 0b10
	FLAGS equ MALIGN | MEMINFO
	MAGIC equ 0x1BADB002
	CKSUM equ -(MAGIC+FLAGS)

	section .multiboot
	align 4
	dd MAGIC
	dd FLAGS
	dd CKSUM
section .bootstrap_stack
align 4
stack_bottom:
times 16384 db 0
stack_top:
	
	section .text
global _start
_start:
	mov esp, stack_top
	call early_main
	cli
stop:
	hlt
	jmp stop
