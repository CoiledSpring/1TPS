#ifndef GDT_H
#define GDT_H
#include <stdint.h>
#define GDT_SIZE 3
#define GDT_ADDRESS 0
// Define for the access byte
#define GDT_PRESENT     1 << 7 | 1 << 4
#define GDT_PRVL_KERNEL 0
#define GDT_PRVL_USER   3 << 5
#define GDT_CODE        1 << 3
#define GDT_DATA        0
#define GDT_GROWUP      0
#define GDT_GROWDOWN    1 << 2
#define GDT_ALLP        1 << 2
#define GDT_ONLYP       0
#define GDT_RW          1 << 1
// Define for the flag nibble
#define GDT_BYTE_GRANULARITY 0
#define GDT_PAGE_GRANULARITY 1<<3
#define GDT_32BIT_MODE 1<<2
#define GDT_16BIT_MODE 0
struct GDTEntry {
    uint16_t limite0_15;
    uint16_t base0_15;
    uint8_t base16_23;
    uint8_t access;
    uint8_t limite16_19 : 4;
    uint8_t flags : 4;
    uint8_t base24_31;
} __attribute__((packed));
struct GDT_Desc {
    uint16_t   size;
    uint32_t * address;
} __attribute__((packed));
void init_gdt_entry(uint32_t base, uint32_t limite, uint8_t access, uint8_t flags, struct GDTEntry *entry);
void install_gdt();
extern struct GDTEntry tgdt[GDT_SIZE];
extern struct GDT_Desc gdtr;
#endif /* GDT_H */
