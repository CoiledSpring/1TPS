void asm_default_int();
void asm_divide_by_zero();
void asm_int_2();
void asm_nmi();

void asm_clock();
void asm_keyboard();

void c_default_int();
void c_divide_by_zero();
void c_int_2();
void c_nmi();

void c_clock();
void c_keyboard();
