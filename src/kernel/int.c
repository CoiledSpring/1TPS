#include <stdio.h>
#include <io.h>
#include "tty.h"
#include "keymaps/us.h"
uint8_t Ctrl_ON;
uint8_t Shift_ON;
void c_default_int()
{
    printf_debug("AN UNKNOWN INTERRUPT OCCURED!!!! WHAT SHOULD I DO?\n");
}
void c_divide_by_zero()
{
    printf_debug("It ain't possible to divide by zero. Y'all know that's what the calculator says, right?\n");
}
void c_int_2()
{
    printf_debug("What the hell? How did you even manage to throw that error? O_o\n");
}
void c_nmi()
{
    printf_debug("A Non-Maskable Interrupt occured. What else? \n");
}
void c_breakpoint()
{
    
}
void c_clock()
{
    //printf_debug("1 tick!\n");
}
void c_keyboard()
{
    uint8_t key, car;
    do
    {
	key = inb(0x64);
    } while((key & 1) == 0);
    key = inb(0x60);
    // printf_debug("Keycode : 0x%x\n", key);
    if(key == 0xE0) {
	do {
	    key = inb(0x64);
	} while((key & 1) == 0);
	key = inb(0x60);
	// printf_debug("Special keycode : 0xEO%x\n", key);
	switch (key)
	{
	case 0x48: // Touche du haut
	    break;
	case 0x4D: // Touche droite
	    break;
	case 0x50: // Touche du bas
	    break;
	case 0x4B:
	    tty_current->tty_x--;
	    if(tty_current->tty_x == 255) { tty_current->tty_y--; tty_current->tty_x = VGA_WIDTH-1; }
	    break;
	}
    }
    if ((key & 0x80) == 0)
    {
	car = kbdmap[(key * 4) + Ctrl_ON + Shift_ON];
	if(car >= 10 && car <= 21)
	{
	    change_tty(car-10);
	    return;
	}
	switch(car)
	{
	case CTRL:
	    Ctrl_ON = 2;
	    break;
	case SHIFT:
	    Shift_ON = 1;
	    break;
	case ENTER:
	    putchar('\n');
	    break;
	default:
	    putchar(car);
	}
    }
    else
    {
	key = kbdmap[(key & 0x7F)*4 + Ctrl_ON + Shift_ON];
	switch(key)
	{
	case CTRL:
	    Ctrl_ON = 0;
	    break;
	case SHIFT:
	    Shift_ON = 0;
	    break;
	}
    }
    
}
