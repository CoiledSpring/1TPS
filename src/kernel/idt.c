#include <string.h>
#include "idt.h"
#include "isr.h"
struct IDTEntry tidt[256];
struct IDT_Desc idtr;
void init_idt_entry(uint32_t base, uint16_t selector, uint8_t type, uint8_t storage_segment, uint8_t dpl, uint8_t present, struct IDTEntry *entry)
{
    entry->base0_15         = (base & 0x0000FFFF);
    entry->selector         = (selector);
    entry->zero             = (0);
    entry->type             = (type & 0x0F);
    entry->storage_segment  = (storage_segment & 0x01);
    entry->dpl              = (dpl & 0x03);
    entry->present          = (present & 0x01);
    entry->base16_31        = (base & 0xFFFF0000) >> 16;
}
void install_idt(uint32_t base)
{
    int i;
    for(i=0; i<256; i++)
	  init_idt_entry((uint32_t)asm_default_int, 0x08, IDT_32BIT_INTGATE, 0, 0, 1, & tidt[i]);
    // Erreurs
    init_idt_entry((uint32_t)asm_divide_by_zero, 0x08, IDT_32BIT_INTGATE, 0, 0, 1, & tidt[0]);
    // Interruptions
    init_idt_entry((uint32_t)asm_clock, 0x08, IDT_32BIT_INTGATE, 0, 0, 1, & tidt[0x20]);
    init_idt_entry((uint32_t)asm_keyboard, 0x08, IDT_32BIT_INTGATE, 0, 0, 1, & tidt[0x21]);
    idtr.address = base;
    idtr.size = 256 * sizeof(struct IDTEntry);

    memcpy((void *)idtr.address, (void *)tidt, idtr.size);

    asm("lidtl (idtr)");
}
