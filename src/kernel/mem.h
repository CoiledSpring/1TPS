#ifndef MEM_H
#define MEM_H
#include <stdint.h>
void setup_paging();
void init_bitmap();
void mark_page_used(uint32_t number);
void mark_page_free(uint32_t number);
uint32_t find_a_free_page();
#endif /* MEM_H */
