#ifndef TTY_H
#include <stdint.h>
#define TTY_H
#define VGA_ADDRESS 0xB8000
#define VGA_WIDTH 80
#define VGA_HEIGHT 25
enum VGA_color {
    black=0,
    blue,
    green,
    cyan,
    red,
    magenta,
    brown,
    light_grey,
    dark_grey,
    light_blue,
    light_green,
    light_cyan,
    light_red,
    light_magenta,
    yellow,
    white
};
struct TTY {
    uint8_t tty_x;
    uint8_t tty_y;
    uint8_t tty_color;
    uint8_t stdin_i;
    uint16_t tty_cursor;
    uint16_t tty_buffer[VGA_WIDTH*VGA_HEIGHT];
    char tty_stdin[250];
};
extern struct TTY ttys[12];
extern struct TTY *tty_current;
extern uint8_t tty_id;
extern uint16_t *tty_memory;
void tty_init();
void putchar(char c);
void move_cursor();
void change_tty(uint8_t nb);
void scrollup();
void tty_actualise();
#endif /* TTY_H */
