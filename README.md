# 1TPS

*1TPS* is a basic kernel I've been writing to familiarise myself with the development of a kernel. It does not do anything spectacular, and most probably never will.

## Building

To build the kernel, you need GCC built to compile for the i686 architecture (*i686-elf-gcc*). You also need nasm.
Then, you only have to type in “make qemu” or “make kvm” to use either qemu, or qemu with kvm.
