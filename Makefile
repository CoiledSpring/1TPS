CC = i686-elf-gcc
AR = i686-elf-ar
CFLAGS = -Wall -Wextra -nostdlib -ffreestanding -O2 -std=gnu11
INCLUDE = -Isrc/kernel/libk -Isrc/kernel/drivers -Iinclude/
LIBK = -L. -lk
DEBUG = -g -ggdb
LIBK_OBJ:=obj/kernel/libk/printf.o \
obj/kernel/libk/strlen.o           \
obj/kernel/libk/itoa.o             \
obj/kernel/libk/reverse.o          \
obj/kernel/libk/memcpy.o           \
obj/kernel/libk/memset.o           \
obj/kernel/libk/printf_debug.o     \

DRIVERS:=obj/kernel/drivers/vga.o  \
obj/kernel/drivers/serial.o        \
obj/kernel/drivers/rtc.o           \
obj/kernel/drivers/cmos.o          \



KERNEL_OBJS:=obj/kernel/main.o     \
obj/kernel/tty.o                   \
obj/kernel/boot.o                  \
obj/kernel/idt.o                   \
obj/kernel/isr.o                   \
obj/kernel/int.o                   \
obj/kernel/gdt.o                   \
obj/kernel/mem.o                   \
obj/kernel/pic.o                   \

all: 	kernel.bin

kernel.bin: 	$(KERNEL_OBJS) $(DRIVERS) libk.a
	$(CC) -T linker.ld -o kernel.bin $(CFLAGS) $(KERNEL_OBJS) $(DRIVERS) -lgcc $(LIBK) $(DEBUG)
libk.a: 	$(LIBK_OBJ)
	$(AR) rcs libk.a $(LIBK_OBJ)
obj/%.o: 	src/%.c
	$(CC) -c $< -o $@ $(CFLAGS) $(INCLUDE) $(DEBUG)
obj/%.o: 	src/%.s
	nasm -f elf32 $< -o $@
clean:
	find -name '*~' -delete -print
	find -name '*.o' -delete -print
	rm -f libk.a
	rm -rf isodir
	rm -f kernel.bin
	rm -f kernel.iso
kernel.iso:	kernel.bin
	rm -rfv isodir
	mkdir isodir
	mkdir isodir/boot
	mkdir isodir/boot/grub
	cp kernel.bin isodir/boot/kernel.bin
	echo "menuentry \"1TPS\" { \
	multiboot /boot/kernel.bin \
	}" > isodir/boot/grub/grub.cfg
	grub-mkrescue -o kernel.iso isodir
kvm:		kernel.bin
	qemu-system-i386 -enable-kvm -serial stdio -s -rtc base=localtime -k en-us -kernel kernel.bin
kvm-iso:	kernel.iso
	qemu-system-i386 -enable-kvm -serial stdio -s -rtc base=localtime -k en-us -cdrom kernel.iso
qemu:		kernel.bin
	qemu-system-i386 -serial stdio -s -rtc base=localtime -k en-us -kernel kernel.bin
qemu-iso:	kernel.iso
	qemu-system-i386 -serial stdio -s -rtc base=localtime -k en-us -cdrom kernel.iso
