import numpy, sys
ccode = """#define CTRL 1
#define SHIFT 2
#define KUP 3
#define KRIGHT 4
#define KDOWN 5
#define KLEFT 6
#define BCKSPACE 127
#define ENTER 8
#define ALT 9
#define F1 10
#define F2 11
#define F3 12
#define F4 13
#define F5 14
#define F6 15
#define F7 16
#define F8 17
#define F9 18
#define F10 19
#define F11 20
#define F12 21
char kbdmap[] = {"""
keymap = numpy.empty(127, dtype=list)
with open(sys.argv[1], "r") as fichier:
    keyfile = fichier.read()
keyfile = keyfile.split('\n')
for ligne in keyfile:
    com = 0
    key = ligne.split(' ')
    for i in key:
        if com or len(i)==0:
            continue
        if i[0] == '#':
            com = 1
            continue
    if key[0] != "keycode":
        continue
    keymap[int(key[1])] = ligne.split()[3:]
for code in range(0,127):
        for i in range(0,4):
            try:
                if keymap[code][i][:3] == "+U+":
                    if int(keymap[code][i][3:], 16) < 128:
                        keymap[code][i] = "'{}'".format(chr(int(keymap[code][i][3:], 16)))
                    else:
                        keymap[code][i] = "' '"
                elif keymap[code][i][:2] == "U+":
                    if int(keymap[code][i][2:], 16) < 128:
                        keymap[code][i] = "'{}'".format(chr(int(keymap[code][i][2:], 16)))
                    else:
                        keymap[code][i] = "' '"
                elif keymap[code][i] == "Control":
                    keymap[code][i] = "CTRL"
                elif keymap[code][i] == "Shift":
                    keymap[code][i] = "SHIFT"
                elif keymap[code][i] == "Up":
                    keymap[code][i] = "KUP"
                elif keymap[code][i] == "Right":
                    keymap[code][i] = "KRIGHT"
                elif keymap[code][i] == "Down":
                    keymap[code][i] = "KDOWN"
                elif keymap[code][i] == "Left":
                    keymap[code][i] = "KLEFT"
                elif keymap[code][i] == "Delete":
                    keymap[code][i] = "BCKSPACE"
                elif keymap[code][i] == "Return":
                    keymap[code][i] = "ENTER"
                elif keymap[code][i] == "Alt":
                    keymap[code][i] = "ALT"
                elif keymap[code][i] == "F1":
                    pass
                elif keymap[code][i] == "F2":
                    pass
                elif keymap[code][i] == "F3":
                    pass
                elif keymap[code][i] == "F4":
                    pass
                elif keymap[code][i] == "F5":
                    pass
                elif keymap[code][i] == "F6":
                    pass
                elif keymap[code][i] == "F7":
                    pass
                elif keymap[code][i] == "F8":
                    pass
                elif keymap[code][i] == "F9":
                    pass
                elif keymap[code][i] == "F10":
                    pass
                elif keymap[code][i] == "F11":
                    pass
                elif keymap[code][i] == "F12":
                    pass
                elif len(keymap[code][i]) > 1:
                    keymap[code][i] = "' '"
                else:
                    keymap[code][i] = "'{}'".format(keymap[code][i])
            except TypeError:
                keymap[code] = []
                keymap[code].append("' '")
            except IndexError:
                keymap[code].append("' '")
            if keymap[code][i] in ["'\\'", "'''", "'\"'"]: keymap[code][i] = "'\\" + keymap[code][i][1:]
            ccode += "{}, ".format(keymap[code][i])
        ccode += "\n"
ccode = ccode[:len(ccode)-3] + '};'
print(ccode)
